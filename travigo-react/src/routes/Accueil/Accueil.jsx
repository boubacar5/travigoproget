import React from 'react';
import { useEffect, useState } from 'react';
import Axios from "axios"
import "./srcAccueil/accueil.css";
import backgroundVideo from './srcAccueil/backgroundVideo.mp4';
import miami from './srcAccueil/miami.jpg';

function Accueil() {

  const [data, setData] = useState([]);
  const [recherche, setRecherche] = useState("");
  const urlVoyages = "voyages.json";

  // filtre les voyages en fonction
  const voyagesFiltres = data.filter((voyage) =>
    voyage.destination.toLowerCase().includes(recherche.toLowerCase())
  );

  
  // Utilisation d'Axios pour recuperer les donnes des voyages 
  useEffect(() => {
    Axios.get(urlVoyages)
    .then(response => {
      console.log("donnees voyages:", response.data)
      setData([...response.data])
    }).catch(error => {
      console.log("Error:", error);
    });
  }, [])



  return (
    <div className='accueil'>
      <div>
        <video src={backgroundVideo} muted autoPlay loop type="video/mp"></video>
      </div>
      <div className='text1 '>
        <span class="">
          <h1>Travigo <br/> Travel</h1>
        </span>
        <span>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. <br/> Aliquam eveniet aut minus quam tempore voluptatem.
          </p>
        </span>
      </div>
      <div className='barRecherche'>
        <div>
          <h2 >Recherchez votre destination!</h2> <br/>
        </div>
        <div>
          <input type="text" placeholder="Destination..." name="destination" onChange={(e)=> setRecherche(e.target.value)}/>
          <input type="date" /> 
          <input type="number" id="price"  min="300" max="6000" step="100" placeholder="Entrez le prix" ></input> <br/>
          <button className='button' type="submit" >Envoye🔍</button>
        </div>
      </div>
      <div>
        <ul>
          {voyagesFiltres.map((voyage) => (
            <li key={voyage.pays}>
              <p>{voyage.description}</p>
              <p>{voyage.prix} €</p>
              <button>Réserver</button>
            </li>
          ))}
        </ul>
            <p>Vous avez recherché: {recherche}</p> 
      </div>
        <div class="destination">
          <h3>Destinations plus visitées:</h3><br/><br/>
          <div class="carteBox">
            <div class="carte">
              <img src={miami} alt="Destination 1"/>
              <h4>Destination 1</h4>
              <p>Description de la destination 1</p>
            </div>
            <div class="carte">
              <img src={miami} alt="Destination 2"/>
              <h4>Destination 2</h4>
              <p>Description de la destination 2</p>
            </div>
            <div class="carte">
              <img src={miami} alt="Destination 3"/>
              <h4>Destination 3</h4>
              <p>Description de la destination 3</p>
            </div>
            <div class="carte">
              <img src={miami} alt="Destination 4"/>
              <h4>Destination 4</h4>
              <p>Description de la destination 4</p>
            </div>
            <div class="carte">
              <img src={miami} alt="Destination 5"/>
              <h4>Destination 5</h4>
              <p>Description de la destination 5</p>
            </div>
            <div class="carte">
              <img src={miami} alt="Destination 6"/>
              <h4>Destination 6</h4>
              <p>Description de la destination 6</p>
            </div>
          </div>
        </div> 
    </div>
  );
}

export default Accueil;

import { Link } from "react-router-dom";
import styled from "styled-components";

const StyledLink = styled(Link)`
  padding: 15px;
  color: #8186a0;
  text-decoration: none;
  font-size: 18px;
`;

const NavContainer = styled.nav`
  padding: 30px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;


function Header() {
  return (
    <NavContainer>
      <div>
        <nav class="navbar navbar-expand-lg navbar-dark  ">
          <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbarColor01">
              <ul class="navbar-nav me-auto">
                <li class="nav-item">
                  <StyledLink to="/">Accueil</StyledLink> 
                </li>
                <li class="nav-item">
                  <StyledLink to="/apropos">A Propos</StyledLink>
                </li>
                <li class="nav-item">
                  <StyledLink to="/nosvoyages">Nos Voyages</StyledLink>
                </li>
                <li class="nav-item">
                  <StyledLink to="/contact">Contact</StyledLink>
                </li>
              </ul>
            </div>
          </div>
          </nav>
      </div>
    </NavContainer>
  );
}

export default Header;

import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
// npm i react-router-dom
import { BrowserRouter as Router } from "react-router-dom";
// npm i react-router
import { Routes, Route as ReactRoute } from "react-router";
import Header from "./components/Header";
import Footer from './components/Footer';
import Accueil from "./routes/Accueil";
import Apropos from './routes/Apropos';
import NosVoyages from './routes/NosVoyeges';
import Contact from './routes/Contact';
import 'bootstrap/dist/css/bootstrap.css';
// import './public/assets/bootstrap.min.css';
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
     <Header/>
      <Routes>
      <ReactRoute path="/" element={<Accueil />}></ReactRoute>
        <ReactRoute path='/apropos/' element={<Apropos/>}></ReactRoute>
        <ReactRoute path='/nosvoyages/' element={<NosVoyages/>}></ReactRoute>
        <ReactRoute path='/contact/' element={<Contact/>}></ReactRoute>
      </Routes>
      <Footer/>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
